# clustering-interface

React and Canvas based interface for psychology experiments.

## Dependencies

* bash and a linux environment (deployment)
* node
* npm
* A browser

## Getting started

Client (Javascript):

* Get the current version of node.js <https://nodejs.org/en/>

* Install dependencies

```
npm install
```

* Start the development server

```
npm start
```

Server (Python & R):

* Install the latest version of Python <https://www.python.org/downloads/>

* Install the latest version of R <https://cran.r-project.org/bin/>

* Setup a UNIX environment if you're on Windows. Instructions for this are a bit more complex. Look into Cygwin or MSYS2.


* Install R dependencies (tidyverse) and Python dependencies (pip install poetry && poetry install)

* Open a UNIX terminal and run the following to run both the client and server servers in one terminal.

```
./development_servers
```

~ Vijay
