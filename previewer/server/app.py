from sanic import Sanic
from sanic import response
import json
import subprocess


def generate_stimuli(input_):
    output = subprocess.check_output(["../../point-generation/build/ground-truth-clusters-json", input_])
    return output.decode()

def generate_random_stimuli(input_):
    output = subprocess.check_output(["../../point-generation/build/random-points-json", input_])
    return output.decode()

app = Sanic(__name__)

@app.route("/api/v1/gen_item", methods=["POST"])
def gen_item(request):
    return response.text(
        generate_stimuli(request.body), headers={"content-type": "application/json"}
    )

@app.route("/api/v1/gen_random_item", methods=["POST"])
def gen_rand_item(request):
    return response.text(
        generate_random_stimuli(request.body), headers={"content-type": "application/json"}
    )
