const path = require("path");

module.exports = {
  mode: "development",
  module: {
    rules: [
      {
        test: /.js$/,
        loader: "babel-loader",
        exclude: /node_modules/,
        options: {
          presets: ["@babel/preset-react", "@babel/preset-env"]
        }
      }
    ]
  },
  devServer: {
    contentBase: path.join(__dirname, "public"),
    proxy: {
      "/api": "http://localhost:8000"
    }
  },
  devtool: "source-map"
};
