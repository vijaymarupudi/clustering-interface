import React, { useEffect, useState, useLayoutEffect, useRef } from "react";
import Tabs from "../Tabs.js";

function RenderPoints({ pointData, formState }) {
  const canvasRef = useRef();

  useLayoutEffect(() => {
    const canvas = canvasRef.current;
    const ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, 800, 500);
    for (const { x, y } of pointData.points) {
      ctx.beginPath();
      ctx.moveTo(x, y);
      ctx.arc(x, y, 5, 0, Math.PI * 2);
      ctx.fill();
    }
  }, [pointData]);

  const updatedSeedSettings = formState && {
    ...formState,
    seed: pointData["seed"]
  };

  const zScore = pointData["z_score"]
  const vacuumedZScore = pointData["vacuumed_z_score"]
  const stdVacuumedZScore = pointData["std_vacuumed_z_score"]

  return (
    <div>
      <div>
        <b>Z score: {zScore}</b>
      </div>
      <div>
        <b>Vacuumed Z score: {vacuumedZScore}</b>
      </div>
      <div>
        <b>Standardized vacuumed Z score: {stdVacuumedZScore}</b>
      </div>
      <div>
        {formState && (
          <div>
            <b>Text settings</b>:{" "}
            <input
              type="text"
              value={btoa(JSON.stringify(updatedSeedSettings))}
              readOnly
              onClick={e => e.target.select()}
            />
          </div>
        )}
        <div>
          <canvas ref={canvasRef} width={800} height={500} />
        </div>
      </div>
    </div>
  );
}

const items = [
  { text: "Seed", name: "seed", defaultValue: 0 },
  {
    text: "Minimum point distance",
    name: "minPointDistance",
    defaultValue: 12
  },
  { text: "Points per cluster", name: "pointsPerCluster", defaultValue: 10 },
  {
    text: "Standard deviation",
    name: "standardDeviation",
    defaultValue: 20,
    step: 5
  },
  { text: "Number of clusters", name: "nClusters", defaultValue: 3 },
  { text: "Canvas width", name: "canvasWidth", defaultValue: 800 },
  { text: "Canvas height", name: "canvasHeight", defaultValue: 500 },
  { text: "Canvas padding", name: "canvasPadding", defaultValue: 100 }
];

function ParameterGenerator() {
  const initialFormState = items.reduce((acc, cur) => {
    acc[cur.name] = cur.defaultValue;
    return acc;
  }, {});
  const [formState, setFormState] = useState(initialFormState);
  const [pointData, setPointData] = useState(null);

  console.log(pointData)

  // useEffect(() => {
  //   if (!formState) return;
  //   (async () => {
  //     const resp = await fetch("/api/v1/gen_item", {
  //       method: "POST",
  //       body: JSON.stringify(formState),
  //       headers: {
  //         "content-type": "application/json"
  //       }
  //     });
  //     const data = await resp.json();
  //     console.log(data);
  //     setPointData(data);
  //   })();
  // }, [formState]);
  //
  //
  async function settingsSubmit(e) {
    const resp = await fetch("/api/v1/gen_item", {
      method: "POST",
      body: JSON.stringify(formState),
      headers: {
        "content-type": "application/json"
      }
    });
    const data = await resp.json();
    setPointData(data);
  }

  return (
    <div>
      <div className="row">
        {items.map(item => (
          <div key={item.name} className="form-group col">
            <label htmlFor={item.name}>{item.text}</label>
            <input
              className="form-control"
              type="number"
              value={formState[item.name]}
              step={item.step}
              onChange={e =>
                setFormState({
                  ...formState,
                  [item.name]: e.target.valueAsNumber
                })
              }
            />
          </div>
        ))}
      </div>
      <div>
        <b>Text settings</b>:{" "}
        <input
          type="text"
          value={btoa(JSON.stringify(formState))}
          onChange={e => setFormState(JSON.parse(atob(e.target.value)))}
          onClick={e => e.target.select()}
        />
      </div>
      <div className="form-group">
        <button className="btn btn-primary" onClick={settingsSubmit}>
          Submit
        </button>
      </div>
      <div className="mb-2">
        {pointData && (
          <RenderPoints pointData={pointData} formState={formState} />
        )}
      </div>
    </div>
  );
}

function Random() {
  const [nPoints, setNPoints] = useState(12);
  const [pointData, setPointData] = useState(null);

  async function onSubmit(e) {
    const resp = await fetch("/api/v1/gen_random_item", {
      method: "POST",
      body: JSON.stringify({ nPoints: nPoints })
    });
    const data = await resp.json()
    console.log(data)
    setPointData(data);
  }

  return (
    <div>
      <div className="form-group">
        <label>Number of points</label>
        <input
          type="number"
          value={nPoints}
          onChange={e => {
            setNPoints(e.target.valueAsNumber);
          }}
          className="form-control"
          step="1"
        />
      </div>
      <div>
        <button className="btn btn-primary" onClick={onSubmit}>
          Submit
        </button>
      </div>
      <div>{pointData && <RenderPoints pointData={pointData} />}</div>
    </div>
  );
}

export default function PointGenerator() {
  return (
    <Tabs
      content={{
        "Parameter based": <ParameterGenerator />,
        Random: <Random />
      }}
    />
  );
}
