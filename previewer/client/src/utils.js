import parse from "csv-parse";

function readFile(file) {
  const fileReader = new FileReader();
  return new Promise((resolve, reject) => {
    fileReader.addEventListener("load", function() {
      resolve(fileReader.result);
    });

    fileReader.addEventListener("error", function() {
      reject(fileReader.error);
    });
    fileReader.readAsText(file);
  });
}

export async function readCsvFile(file) {
  const fileContents = await readFile(file);
  return new Promise((resolve, reject) => {
    parse(fileContents, function(err, parsed) {
      if (err) reject(err);
      else resolve(parsed);
    });
  });
}

export function triggerFileDownload(filename, mime, text) {
  const dataURI = "data:" + mime + "," + text;
  const linkElement = document.createElement("a");
  linkElement.href = dataURI;
  linkElement.setAttribute("download", filename);
  document.body.appendChild(linkElement);
  linkElement.click();
  document.body.removeChild(linkElement);
}

export function drawCoordinates(
  canvas,
  coordinates,
  radius = 5,
  pathRadius = 8
) {
  const ctx = canvas.getContext("2d");
  return coordinates.map(coordinateInfo => {
    const { x, y } = coordinateInfo;
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, Math.PI * 2);
    ctx.fill();
    // larger hitbox for easier clicking
    const path = new Path2D();
    path.arc(x, y, pathRadius, 0, Math.PI * 2);
    return { ...coordinateInfo, path: path };
  });
}

export function getXY(canvas, event) {
  return [event.pageX - canvas.offsetLeft, event.pageY - canvas.offsetTop];
}
