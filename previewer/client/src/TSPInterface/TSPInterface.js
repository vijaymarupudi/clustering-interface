import React, { useState, useRef, useEffect } from "react";
import { uniqueId } from "lodash-es";
import { readCsvFile, drawCoordinates, getXY } from "../utils";

function PointAndPathRenderer({ coordinates, dimensions, tspDataState }) {
  const canvasRef = useRef();
  const coordinateIdInfoRef = useRef({});
  const remainingCoordinateIdInfoRef = useRef({});

  const [tspData, setTspData] = tspDataState;

  useEffect(() => {
    /** @type {CanvasRenderingContext2D} */
    const ctx = canvasRef.current.getContext("2d");
    ctx.clearRect(0, 0, canvasRef.current.width, canvasRef.current.height);
    const coordinatesWithPath = drawCoordinates(canvasRef.current, coordinates);
    coordinateIdInfoRef.current = coordinatesWithPath.reduce((acc, cur) => {
      acc[cur.id] = { ...cur };
      return acc;
    }, {});
  });

  // this should not update all the time, especially when tspData updates.
  // it's programmed a bit messily, look in refactoring this when major changes are needed.
  // use tsp data directly instread of remainingCoordinateIdInfoRef.

  useEffect(() => {
    remainingCoordinateIdInfoRef.current = coordinateIdInfoRef.current;
  }, [coordinates, dimensions]);

  useEffect(() => {
    const ctx = canvasRef.current.getContext("2d");
    const pointIdsNotClicked = Array.from(
      Object.keys(remainingCoordinateIdInfoRef.current)
    );

    const clickedCoordinates = coordinates.filter(
      coor => !pointIdsNotClicked.includes(coor.id)
    );
    ctx.save();
    ctx.fillStyle = "#00b7be";
    drawCoordinates(canvasRef.current, clickedCoordinates);
    ctx.restore();
  });

  useEffect(() => {
    const ctx = canvasRef.current.getContext("2d");
    ctx.save();
    ctx.lineWidth = 2;
    ctx.beginPath();
    for (const { pointId } of tspData) {
      const { x, y } = coordinateIdInfoRef.current[pointId];
      ctx.lineTo(x, y);
    }
    if (Object.keys(remainingCoordinateIdInfoRef.current).length === 0) {
      ctx.closePath();
    }
    ctx.stroke();
    ctx.restore();
    ctx.beginPath();
  });

  useEffect(() => {
    const ctx = canvasRef.current.getContext("2d");
    const clickListener = e => {
      const [x, y] = getXY(canvasRef.current, e);
      let clickedPointId = null;
      for (const [key, value] of Object.entries(
        remainingCoordinateIdInfoRef.current
      )) {
        // value is of type Path2D
        if (ctx.isPointInPath(value.path, x, y)) {
          clickedPointId = key;
          break;
        }
      }
      if (!clickedPointId) return;

      delete remainingCoordinateIdInfoRef.current[clickedPointId];

      setTspData([
        ...tspData,
        { timestamp: performance.now(), pointId: clickedPointId }
      ]);
    };
    canvasRef.current.addEventListener("click", clickListener);
    return () => {
      canvasRef.current.removeEventListener("click", clickListener);
    };
  });

  return (
    <canvas
      ref={canvasRef}
      width={dimensions.width}
      height={dimensions.height}
      style={{ border: "1px solid black" }}
    />
  );
}

export default function TSPInterface() {
  const [coordinates, setCoordinates] = useState(null);
  const [tspData, setTspData] = useState([]);
  const [dimensions, setDimensions] = useState({ width: 500, height: 500 });
  return (
    <div>
      <div className="mb-2">
        <input
          type="file"
          className="file-control-file"
          onChange={async e => {
            const file = e.target.files[0];
            let parsedCsv = await readCsvFile(file);
            parsedCsv.splice(0, 1);
            parsedCsv = parsedCsv.map(([x, y]) => ({
              x: parseFloat(x) * dimensions.width,
              y: parseFloat(y) * dimensions.height,
              id: uniqueId()
            }));
            setCoordinates(parsedCsv);
          }}
        />
      </div>

      {coordinates && (
        <PointAndPathRenderer
          coordinates={coordinates}
          dimensions={dimensions}
          tspDataState={[tspData, setTspData]}
        />
      )}
    </div>
  );
}
