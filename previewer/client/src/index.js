import "@babel/polyfill";
import React from "react";
import ReactDOM from "react-dom";
import { HashRouter, Route, NavLink } from "react-router-dom";

import ClusteringInterface from "./ClusteringInterface";
import PointEditor from "./PointEditor";
import TSPInterface from "./TSPInterface";
import PointGenerator from "./PointGenerator";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <ul className="navbar-nav">
        <li className="nav-item">
          <NavLink to="/" exact activeClassName="active" className="nav-link">
            Clustering Interface
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink
            to="/tsp-interface"
            activeClassName="active"
            className="nav-link"
          >
            TSP Interface
          </NavLink>
        </li>

        <li className="nav-item">
          <NavLink
            to="/point-editor"
            activeClassName="active"
            className="nav-link"
          >
            Point editor
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink
            to="/point-generator"
            activeClassName="active"
            className="nav-link"
          >
            Point generator
          </NavLink>
        </li>
      </ul>
    </nav>
  );
}

function App() {
  return (
    <HashRouter>
      <Nav />
      <div className="p-3">
        <Route path="/" exact={true} component={ClusteringInterface} />
        <Route path="/point-editor" component={PointEditor} />
        <Route path="/tsp-interface" component={TSPInterface} />
        <Route path="/point-generator" component={PointGenerator} />
      </div>
    </HashRouter>
  );
}

ReactDOM.render(<App />, document.getElementById("root"));
