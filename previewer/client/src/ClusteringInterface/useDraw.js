import { useRef, useEffect } from "react";

export default function useDraw(canvasRef, settings, onChange) {
  const allRawDataRef = useRef([]);

  // for private use within function
  const mouseDrawingStatusRef = useRef();
  const currentPointsRef = useRef([]);
  const currentPathRef = useRef();

  function getEventCoordinates(event) {
    const x = event.pageX - canvasRef.current.offsetLeft;
    const y = event.pageY - canvasRef.current.offsetTop;
    return [x, y];
  }

  function addDrawPoint(event) {
    const [x, y] = getEventCoordinates(event);
    currentPointsRef.current.push({
      x: x,
      y: y,
      timestamp: performance.now()
    });
    currentPathRef.current.lineTo(x, y);
    canvasRef.current.getContext("2d").stroke(currentPathRef.current);
  }

  useEffect(() => {
    const canvas = canvasRef.current;
    const ctx = canvasRef.current.getContext("2d");
    ctx.lineWidth = 5;

    const mouseDownFunc = function(e) {
      mouseDrawingStatusRef.current = true;

      ctx.save(); // for color and thickness
      ctx.lineWidth = settings.clusterLineThickness;
      ctx.strokeStyle = settings.clusterDrawingColor;

      currentPathRef.current = new Path2D();
      addDrawPoint(e);
    };

    const mouseMoveFunc = function(e) {
      if (!mouseDrawingStatusRef.current) return;
      addDrawPoint(e);
    };

    const mouseUpFunc = function() {
      if (!mouseDrawingStatusRef.current) return;
      mouseDrawingStatusRef.current = false;
      currentPathRef.current.closePath();
      ctx.stroke(currentPathRef.current);
      ctx.restore(); // reset color and thickness

      allRawDataRef.current.push({
        points: currentPointsRef.current,
        path: currentPathRef.current
      });

      currentPointsRef.current = [];

      onChange(allRawDataRef.current);
    };

    document.addEventListener("mousemove", mouseMoveFunc);
    canvas.addEventListener("mousedown", mouseDownFunc);
    document.addEventListener("mouseup", mouseUpFunc);

    return () => {
      document.removeEventListener("mousemove", mouseMoveFunc);
      canvas.removeEventListener("mousedown", mouseDownFunc);
      document.removeEventListener("mouseup", mouseUpFunc);
    };
  });
}
