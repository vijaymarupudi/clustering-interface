import React, { useRef, useEffect, useState } from "react";
import { cloneDeep } from "lodash-es";
import { readCsvFile, triggerFileDownload } from "../utils";
import Tabs from "../Tabs.js";

import useDraw from "./useDraw";

function useCoordinates(coordinates, rawData, settings, canvasRef) {
  useEffect(() => {
    if (!coordinates) return;
    const ctx = canvasRef.current.getContext("2d");
    const clusterPaths = rawData.map(x => x.path);
    ctx.save();
    for (const { x, y } of coordinates) {
      ctx.save();
      ctx.beginPath();
      if (settings.displayClusteredStatus) {
        const pointInCluster = clusterPaths.some(path =>
          ctx.isPointInPath(path, x, y)
        );
        if (pointInCluster) ctx.fillStyle = settings.clusterDrawingColor;
      }
      ctx.arc(x, y, settings.pointRadius, 0, Math.PI * 2);
      ctx.fill();
      ctx.restore();
    }
    ctx.restore();
    ctx.beginPath();
  }, [coordinates, rawData]);
}

function RawDataViewer({ data: rawData, coordinates, ...props }) {
  // creating this just to use the isPointInPath method.
  const ctx = document.createElement("canvas").getContext("2d");
  let remainingPoints = cloneDeep(coordinates);

  const processedData = {};
  processedData.rawData = rawData;
  processedData.clusterInfo = [];

  for (const cluster of rawData) {
    const path = cluster.path;
    processedData.clusterInfo.push({
      pointsInCluster: remainingPoints.filter(({ x, y }) =>
        ctx.isPointInPath(path, x, y)
      )
    });
    remainingPoints = remainingPoints.filter(
      ({ x, y }) => !ctx.isPointInPath(path, x, y)
    );
  }

  function download() {
    triggerFileDownload(
      new Date().valueOf() + "_data.json",
      "application/json",
      JSON.stringify(processedData)
    );
  }

  return (
    <div {...props}>
      Clusters: {rawData.length}
      <br />
      Number of points: {coordinates.length} <br />
      Remaining points: {remainingPoints.length}
      <details>
        <summary>
          Raw Data{" "}
          <button className="btn btn-primary" onClick={download}>
            Download
          </button>
        </summary>
        <pre>
          <code>{JSON.stringify(rawData, null, 2)}</code>
        </pre>
      </details>
    </div>
  );
}

function ClusteringCanvas({ settings }) {
  const { height, width, coordinates } = settings;
  const canvasRef = useRef();
  const [rawData, setRawData] = useState([]);

  useCoordinates(coordinates, rawData, settings, canvasRef);
  useDraw(canvasRef, settings, function(incomingRawData) {
    setRawData(cloneDeep(incomingRawData));
  });

  return (
    <div style={{ display: "flex" }}>
      <canvas
        style={{ border: "1px solid black" }}
        className="mr-2"
        width={width}
        height={height}
        ref={canvasRef}
      />
      {coordinates && (
        <RawDataViewer data={rawData} coordinates={coordinates} />
      )}
    </div>
  );
}

function SettingsForm({ settings, setSettings }) {
  return (
    <div>
      <div className="form-group">
        <label className="mr-1">
          Width (px)
          <input
            className="form-control"
            type="number"
            value={settings.width}
            onChange={e =>
              setSettings({ ...settings, width: parseInt(e.target.value) })
            }
          />
        </label>
        <label>
          Height (px)
          <input
            className="form-control"
            value={settings.height}
            type="number"
            onChange={e =>
              setSettings({ ...settings, height: parseInt(e.target.value) })
            }
          />
        </label>
      </div>
      <div className="form-group">
        <label>
          Coordinates file{" "}
          <input
            type="file"
            className="form-control-file"
            onChange={async e => {
              const file = e.target.files[0];
              let parsedCsv = cloneDeep(await readCsvFile(file));
              parsedCsv.splice(0, 1); // remove header

              // this block adjusts the csv to make it look like how R would graph it. It would primarily affect the y axis. y = 0 in R is near the bottom of the graph, but y = 0 in canvas is near the top, so I'm going to invert it.

              // remove this if the R code is changed to accomdate this discrepancy.
              parsedCsv.map(([x, y]) => {
                return [x, 1 - y];
              });

              // adjust coordinates according to actual width and height
              parsedCsv = parsedCsv.map(([x, y]) => [
                x * settings.width,
                y * settings.height
              ]);

              // make coordinates into objects
              parsedCsv = parsedCsv.map(([x, y]) => ({
                x: x,
                y: y
              }));

              setSettings({
                ...settings,
                coordinates: parsedCsv,
                coordinatesFile: file
              });
            }}
          />
        </label>
      </div>
      <div className="row form-group">
        <div className="col">
          <label className="mr-1">Cluster drawing color</label>
          <input
            type="text"
            className="form-control"
            value={settings.clusterDrawingColor}
            onChange={e => {
              setSettings({ ...settings, clusterDrawingColor: e.target.value });
            }}
          />
        </div>
        <div className="col">
          <label>Cluster line thickness</label>
          <input
            type="number"
            className="form-control"
            value={settings.clusterLineThickness}
            onChange={e => {
              setSettings({
                ...settings,
                clusterLineThickness: e.target.valueAsNumber
              });
            }}
          />
        </div>
        <div className="col">
          <label>Point radius</label>
          <input
            type="number"
            value={settings.pointRadius}
            className="form-control"
            onChange={e => {
              setSettings({ ...settings, pointRadius: e.target.valueAsNumber });
            }}
          />
        </div>
      </div>
      <div className="form-group">
        <div className="form-check">
          <input
            type="checkbox"
            className="form-check-input"
            id="display-clustered-status"
            checked={settings.displayClusteredStatus}
            onChange={() => {
              setSettings({
                ...settings,
                displayClusteredStatus: !settings.displayClusteredStatus
              });
            }}
          />
          <label
            className="mr-1 form-check-label"
            htmlFor="display-clustered-status"
          >
            Display point clustered status
          </label>
          <small className="form-text text-muted">
            The points will change to the color of the cluster line if
            clustered.
          </small>
        </div>
      </div>
    </div>
  );
}

export default function ClusteringInterface() {
  const [settings, setSettings] = useState({
    width: 500,
    height: 500,
    coordinates: null,
    coordinatesFile: "",
    clusterDrawingColor: "#0000FF",
    clusterLineThickness: 5,
    displayClusteredStatus: true,
    pointRadius: 5
  });

  return (
    <div>
      <div className="pb-2">
        <h3>Clustering Interface</h3>
      </div>
      <Tabs
        content={{
          Home: <ClusteringCanvas settings={settings} />,
          Settings: (
            <SettingsForm settings={settings} setSettings={setSettings} />
          )
        }}
      />
    </div>
  );
}
