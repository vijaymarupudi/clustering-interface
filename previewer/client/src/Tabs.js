import React, { useState } from "react";

export default function Tabs({ content }) {
  const tabTitles = Array.from(Object.keys(content));
  const [tab, setTab] = useState(tabTitles[0]);

  const tabContent = content[tab];

  return (
    <React.Fragment>
      <ul className="nav nav-tabs mb-3">
        {tabTitles.map(title => (
          <li key={title} className="nav-item">
            <a
              style={{ cursor: "pointer" }}
              className={`nav-link ${tab === title ? "active" : ""}`}
              onClick={() => setTab(title)}
            >
              {title}
            </a>
          </li>
        ))}
      </ul>
      <div>{tabContent}</div>
    </React.Fragment>
  );
}
