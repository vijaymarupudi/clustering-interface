import React, { useRef, useEffect, useState } from "react";
import {
  readCsvFile,
  triggerFileDownload,
  drawCoordinates,
  getXY
} from "../utils";
import csvStringify from "csv-stringify/lib/sync";

export default function PointEditor() {
  const canvasRef = useRef();
  const coordinatesRef = useRef();
  const movingIndexRef = useRef(null);
  const [dimensions, setDimensions] = useState({ width: 500, height: 500 });
  const [coordinatesGiven, setCoordinatesGiven] = useState(false);

  // adds a path for each point.
  useEffect(() => {
    if (!coordinatesRef.current) return;
    coordinatesRef.current = drawCoordinates(
      canvasRef.current,
      coordinatesRef.current
    );
  }, [coordinatesGiven]);

  useEffect(() => {
    if (!canvasRef.current) return;
    const canvas = canvasRef.current;
    const ctx = canvas.getContext("2d");

    const mouseDownListener = function(e) {
      const [x, y] = getXY(canvas, e);
      const index = coordinatesRef.current.findIndex(function(coordinate) {
        return ctx.isPointInPath(coordinate.path, x, y);
      });
      if (index != -1) {
        movingIndexRef.current = index;
      }
    };

    const mouseUpListener = function(e) {
      movingIndexRef.current = null;
    };

    const mouseMoveListener = function(e) {
      if (movingIndexRef.current === null) return;
      const [x, y] = getXY(canvas, e);
      const index = movingIndexRef.current;
      coordinatesRef.current[index].x = x;
      coordinatesRef.current[index].y = y;
      canvasRef.current
        .getContext("2d")
        .clearRect(0, 0, dimensions.width, dimensions.height);
      coordinatesRef.current = drawCoordinates(
        canvasRef.current,
        coordinatesRef.current
      );
    };

    canvas.addEventListener("mousedown", mouseDownListener);
    canvas.addEventListener("mousemove", mouseMoveListener);
    canvas.addEventListener("mouseup", mouseUpListener);

    return () => {
      canvas.removeEventListener("mousedown", mouseDownListener);
      canvas.removeEventListener("mousemove", mouseMoveListener);
      canvas.removeEventListener("mouseup", mouseUpListener);
    };
  });

  return (
    <div>
      <h3>Point editor</h3>

      <label>
        Input
        <input
          onChange={async e => {
            const file = e.target.files[0];
            let parsed = await readCsvFile(file);
            // remove header
            parsed.splice(0, 1);
            // adjust for height and width
            parsed = parsed.map(([x, y]) => [
              x * dimensions.width,
              y * dimensions.height
            ]);
            // make into object for storing canvas paths in the future
            parsed = parsed.map(([x, y]) => ({ x: x, y: y }));
            coordinatesRef.current = parsed;
            setCoordinatesGiven(true);
          }}
          className="form-control-file"
          type="file"
        />
      </label>

      {coordinatesGiven && (
        <React.Fragment>
          <div style={{ display: "flex" }}>
            <canvas
              ref={canvasRef}
              width={dimensions.width}
              height={dimensions.height}
              style={{ border: "1px solid black" }}
              className="mr-3"
            />
            <div>
              <button
                onClick={() => {
                  const csvRecords = [["x", "y"]];
                  for (const coordinateInfo of coordinatesRef.current) {
                    csvRecords.push([
                      coordinateInfo.x / dimensions.width,
                      coordinateInfo.y / dimensions.height
                    ]);
                  }
                  // using base64 to preserve newlines
                  const csvString = csvStringify(csvRecords);
                  triggerFileDownload(
                    "new_points.csv",
                    "text/csv;base64",
                    btoa(csvString)
                  );
                }}
                className="btn btn-primary"
              >
                Export csv
              </button>
            </div>
          </div>
        </React.Fragment>
      )}
    </div>
  );
}
