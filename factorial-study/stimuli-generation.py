import csv
import os
import random
from copy import deepcopy

NUMBERS = [7, 9, 11, 13]
CONTROL_NUMBERS = [4, 5, 6, 7]
ORDERS = ["asc", "desc"]
ARITHMETICS = ["+", "x"]


class OverLoopError(Exception):
    pass


def make_experiment_stimulus(number, arithmetic, order):

    target = number if order == "asc" else 1
    increment = 1 if order == "asc" else -1
    start = 1 if order == "asc" else number
    list_of_numbers = [start]

    new_number = start
    while True:
        new_number = new_number + increment
        list_of_numbers.append(new_number)
        if new_number == target:
            break

    return arithmetic.join(str(n) for n in list_of_numbers)


def make_control_stimulus(number, arithmetic):
    number_of_numbers = (number * 2) - 1
    list_of_numbers = []
    for _ in range(number_of_numbers):
        list_of_numbers.append(number)
    return arithmetic.join(str(n) for n in list_of_numbers)


def make_csv(block_data, filename):
    with open(os.path.join("/tmp", filename), "w", newline="") as f:
        writer = csv.DictWriter(f, block_data[0].keys())
        writer.writeheader()
        for row in block_data:
            writer.writerow(row)


def make_number_order_combos(arithmetic):
    for number in NUMBERS:
        for order in ORDERS:
            yield number, order, "EXPERIMENTAL", arithmetic


def get_repeat_indices(list_):
    # Assumes first thing in tuple is number
    numbers = [i[0] for i in list_]
    repeat_indices = []
    for i in range(1, len(numbers)):
        previous = numbers[i - 1]
        current = numbers[i]
        if previous == current:
            repeat_indices.append(i)
    return repeat_indices


def insert_control_at_index(index, insert_into_list, controls_set, arithmetic):
    # This can get into impossible situations sometimes, error out when that happens.
    loop_count = 0
    while True:
        loop_count += 1
        if loop_count > 10000:
            raise OverLoopError(
                "Impossible randomization sequence situation reached. Please retry."
            )
        control_number_to_insert = random.choice(tuple(controls_set))

        if index > 0:
            if insert_into_list[index - 1][0] == control_number_to_insert:
                continue
        if index < len(insert_into_list) - 1:
            if insert_into_list[index][0] == control_number_to_insert:
                continue

        insert_into_list.insert(
            index, (control_number_to_insert, None, "CONTROL", arithmetic)
        )
        controls_set.remove(control_number_to_insert)
        break


def make_block(arithmetic):

    number_order_combos = list(make_number_order_combos(arithmetic))
    repeat_indices = []

    while True:
        random.shuffle(number_order_combos)
        repeat_indices = get_repeat_indices(number_order_combos)
        if len(repeat_indices) <= 1:
            break

    pre_block_data = deepcopy(number_order_combos)

    number_of_controls_left_to_insert = 4
    controls_left_to_insert = set(CONTROL_NUMBERS)

    for idx in repeat_indices:
        insert_control_at_index(
            idx, pre_block_data, controls_left_to_insert, arithmetic
        )
        number_of_controls_left_to_insert -= 1

    for _ in range(number_of_controls_left_to_insert):
        index_to_insert = random.randint(0, len(pre_block_data) - 1)
        insert_control_at_index(
            index_to_insert, pre_block_data, controls_left_to_insert, arithmetic
        )

    final_block_data = []
    for number, order, group, _ in pre_block_data:
        stimulus = None
        if group == "EXPERIMENTAL":
            stimulus = make_experiment_stimulus(number, arithmetic, order)
        else:
            stimulus = make_control_stimulus(number, arithmetic)

        final_block_data.append(
            {
                "number": number,
                "arithmetic": arithmetic,
                "order": order,
                "stimulus": stimulus,
                "group": group,
            }
        )

    return final_block_data


def ensure_make_block(arithmetic):
    while True:
        try:
            return make_block(arithmetic)
        except OverLoopError:
            pass


make_csv(ensure_make_block("+"), "psychopy-addition.csv")
make_csv(ensure_make_block("x"), "psychopy-multiplication.csv")
