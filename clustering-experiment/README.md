A few reminders from lessons learned the hard way.

* Make sure to increase the maximum request size in nginx to over 50M. The data acquired from the experiment is large.
* Make sure the data directory is writeable by the linux user that's running the code
