import React, { useState, useEffect, useLayoutEffect, useRef } from "react";
import PropTypes from "prop-types";
import ScreenCentered from "./ScreenCentered.js";

const Loading = () => (
  <ScreenCentered>
    <h2>Loading...</h2>
  </ScreenCentered>
);

function getXY(event, canvas) {
  const x = event.clientX - canvas.offsetLeft;
  const y = event.clientY - canvas.offsetTop;
  return [x, y];
}

// ClusterStimulus onDone data format:
//
// startDateTime: string,
// startTimestamp: string,
// endTimestamp: string,
// stimulus: ...,
// clusters: [{
//   uuid: string,
//   edgePoints: [...],
//   pointMembership: [{ point, member: boolean }]
// }]
function ClusterStimulus({ stimulus, onDone, onSingleCluster, ...props }) {
  useEffect(() => {
    console.info(`Stimulus ${stimulus["base_uuid"]}`);
  }, []);

  const [complete, setComplete] = useState(null); // this state variable will hold all the data when the trial is over.

  // For graphics and data calculation
  const canvasRef = useRef();
  const currentlyDrawingRef = useRef(false);
  const clusterPathsRef = useRef([]);
  const currentClusterPathRef = useRef(new Path2D());

  // Data
  const metadata = useRef({
    startDateTime: Date.now(),
    startTimestamp: performance.now(),
    endTimestamp: null
  });
  const clustersDataRef = useRef([]);
  const currentClusterPointsDataRef = useRef([]);

  function addClusterPoint(x, y) {
    currentClusterPointsDataRef.current.push({
      x,
      y,
      timestamp: performance.now()
    });
  }

  function finishClusterPoint(x, y) {
    addClusterPoint(x, y);
    clustersDataRef.current.push(currentClusterPointsDataRef.current);
    currentClusterPointsDataRef.current = [];
  }

  useLayoutEffect(() => {
    if (canvasRef.current) {
      const canvas = canvasRef.current;
      const ctx = canvas.getContext("2d");
      ctx.clearRect(0, 0, 800, 500);
    }
  }, [stimulus]);

  useLayoutEffect(() => {
    if (canvasRef.current) {
      const canvas = canvasRef.current;
      const ctx = canvas.getContext("2d");
      ctx.lineWidth = 5;
      for (const { x, y } of stimulus.points) {
        ctx.beginPath();
        ctx.arc(x, y, 5, 0, Math.PI * 2);
        ctx.fill();
      }
      ctx.beginPath();
    }
  }, [canvasRef.current, stimulus]);

  useEffect(() => {
    if (canvasRef.current) {
      const canvas = canvasRef.current;
      const ctx = canvas.getContext("2d");

      const recolorPoints = () => {
        for (const { x, y } of stimulus.points) {
          for (const path of clusterPathsRef.current) {
            if (ctx.isPointInPath(path, x, y)) {
              ctx.save();
              ctx.fillStyle = "#0000FF";
              ctx.beginPath();
              ctx.arc(x, y, 5, 0, Math.PI * 2);
              ctx.fill();
              ctx.restore();
            }
          }
        }
      };

      const completionCheck = () => {
        const everyPointInSomePath = stimulus.points.every(({ x, y }) => {
          return clusterPathsRef.current.some(path =>
            ctx.isPointInPath(path, x, y)
          );
        });

        if (everyPointInSomePath) {
          metadata.current.endTimestamp = performance.now();
          const completionData = metadata.current;
          completionData.stimulus = stimulus;
          completionData.clusters = [];
          for (let i = 0; i < clustersDataRef.current.length; i++) {
            const ctx = canvasRef.current.getContext("2d");
            const clusterPath = clusterPathsRef.current[i];
            const clusterInfo = {
              edgePoints: clustersDataRef.current[i],
              pointMembership: stimulus.points.map(point => ({
                point: point,
                member: ctx.isPointInPath(clusterPath, point.x, point.y)
              }))
            };
            completionData.clusters.push(clusterInfo);
          }

          // Prevent single clusters
          if (completionData.clusters.length === 1) {
            onSingleCluster();
          } else {
            setComplete(completionData);
          }
        }
      };

      const mouseDownListener = e => {
        currentlyDrawingRef.current = true;
        const [x, y] = getXY(e, canvas);
        addClusterPoint(x, y);
        ctx.beginPath();
        ctx.moveTo(x, y);
        currentClusterPathRef.current.moveTo(x, y);
      };

      const mouseMoveListener = e => {
        if (currentlyDrawingRef.current) {
          const [x, y] = getXY(e, canvas);
          addClusterPoint(x, y);
          ctx.lineTo(x, y);
          ctx.stroke();
          currentClusterPathRef.current.lineTo(x, y);
        }
      };

      const mouseUpListener = e => {
        const [x, y] = getXY(e, canvas);
        finishClusterPoint(x, y);
        ctx.closePath();
        ctx.stroke();
        currentClusterPathRef.current.closePath(x, y);
        clusterPathsRef.current.push(currentClusterPathRef.current);
        currentClusterPathRef.current = new Path2D();
        currentlyDrawingRef.current = false;
        recolorPoints();
        completionCheck();
      };

      document.addEventListener("mousedown", mouseDownListener);
      document.addEventListener("mousemove", mouseMoveListener);
      document.addEventListener("mouseup", mouseUpListener);

      return () => {
        document.removeEventListener("mousedown", mouseDownListener);
        document.removeEventListener("mousemove", mouseMoveListener);
        document.removeEventListener("mouseup", mouseUpListener);
      };
    }
  });

  if (complete) {
    return (
      <ScreenCentered>
        <p>
          The trial was completed.{" "}
          <button className="btn btn-primary" onClick={() => onDone(complete)}>
            Proceed to next trial.
          </button>
        </p>
      </ScreenCentered>
    );
  }

  return (
    <canvas
      ref={canvasRef}
      width={800}
      height={500}
      {...props}
      style={{ border: "1px solid black" }}
    />
  );
}

ClusterStimulus.propTypes = {
  stimulus: PropTypes.object.isRequired,
  onDone: PropTypes.func.isRequired
};

function Trial({ stimulus, onDone }) {
  const [start, setStart] = useState(false);
  const [singleCluster, setSingleCluster] = useState(false);

  if (!start) {
    return (
      <ScreenCentered>
        <button className="btn btn-primary" onClick={() => setStart(true)}>
          Start trial.
        </button>
      </ScreenCentered>
    );
  }

  if (singleCluster) {
    return (
      <ScreenCentered>
        <h1>
          <b>WARNING: PLEASE USE MORE THAN ONE CLUSTER.</b>
        </h1>
        <p>
          <button
            className="btn btn-primary"
            onClick={() => {
              setSingleCluster(false);
            }}
          >
            Continue.
          </button>
        </p>
      </ScreenCentered>
    );
  }

  return (
    <ScreenCentered>
      <ClusterStimulus
        stimulus={stimulus}
        onDone={onDone}
        onSingleCluster={() => {
          setSingleCluster(true);
        }}
      />
    </ScreenCentered>
  );
}

Trial.propTypes = {
  stimulus: PropTypes.object.isRequired,
  onDone: PropTypes.func.isRequired
};

function BlockTrialDispatcher({ blockStimuliList, blockStimuli, onDone }) {
  const [currentStimulusIndex, setCurrentStimulusIndex] = useState(0);
  const trialsData = useRef({}); // uniqueUuid: data

  useEffect(() => {
    console.info(`Trial ${currentStimulusIndex + 1}`);
  }, [currentStimulusIndex]);

  function trialCompleted(trialData) {
    trialsData.current[blockStimuliList[currentStimulusIndex]] = trialData;
    if (currentStimulusIndex < blockStimuliList.length - 1) {
      setCurrentStimulusIndex(currentStimulusIndex + 1);
    } else {
      onDone(trialsData.current);
    }
  }

  // keying Trial with uuid because we want all the Refs and States to refresh
  // with each stimulus. While this can be done in a more finessed way
  // (useEffect for each thing), this is easy enough for the complexity we
  // require.
  return (
    <Trial
      key={currentStimulusIndex}
      stimulus={blockStimuli[blockStimuliList[currentStimulusIndex]]}
      onDone={trialCompleted}
    />
  );
}

BlockTrialDispatcher.propTypes = {
  blockStimuliList: PropTypes.array.isRequired,
  blockStimuli: PropTypes.object.isRequired,
  onDone: PropTypes.func.isRequired
};
export default function Block({ blockNumber, setNumber, onDone, practice }) {
  const [blockStimuliList, setBlockStimuliList] = useState(null);
  const [blockStimuli, setBlockStimuli] = useState(null);

  useEffect(() => {
    console.info(`Block ${blockNumber}`);
  }, []);

  useEffect(() => {
    (async function() {
      const requestUrl = practice
        ? "/api/practice-set"
        : "/api/set/" + setNumber + "/" + blockNumber;
      const resp = await fetch(requestUrl);
      const tempBlockStimuliList = await resp.json();

      const tempBlockStimuli = {};

      async function retrieveStimulus(objToStore, uniqueUuid) {
        const resp = await fetch("/api/stimuli/" + uniqueUuid);
        const stimulus = await resp.json();
        objToStore[uniqueUuid] = stimulus;
      }

      await Promise.all(
        tempBlockStimuliList.map(uniqueUuid =>
          retrieveStimulus(tempBlockStimuli, uniqueUuid)
        )
      );

      setBlockStimuliList(tempBlockStimuliList);
      setBlockStimuli(tempBlockStimuli);
    })();
  }, []);

  return blockStimuli ? (
    <BlockTrialDispatcher
      blockStimuliList={blockStimuliList}
      blockStimuli={blockStimuli}
      onDone={function (data) {
        onDone({ blockStimuliList: blockStimuliList, blockData: data });
      }}
    />
  ) : (
    <Loading />
  );
}

Block.propTypes = {
  blockNumber: PropTypes.number,
  setNumber: PropTypes.number,
  onDone: PropTypes.func,
  practice: PropTypes.bool
};
