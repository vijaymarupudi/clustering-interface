import React from "react";

export default function ScreenCentered(props) {
  return (
    <div style={{ height: "100vh", display: "flex" }}>
      <div style={{ margin: "auto" }} {...props} />
    </div>
  );
}
