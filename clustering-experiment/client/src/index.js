import "core-js/stable";
import "regenerator-runtime/runtime";
import React, { useState, useRef } from "react";
import PropTypes from "prop-types";
import ReactDOM from "react-dom";
import ScreenCentered from "./ScreenCentered.js";
import Block from "./Block";

import "../node_modules/bootstrap/dist/css/bootstrap.css";

function Instructions({ onNext }) {
  return (
    <ScreenCentered>
      <div style={{ maxWidth: 1000 }}>
        <h1>Instructions</h1>
        <p>
          You will see some points displayed on the screen each trial. You task
          will be to draw circles around clusters of points. You can draw as
          many circles as you see fit. However, you will need to draw more than
          one circle per trial. The trial will end when you have drawn circles
          around all the dots.
        </p>
        <p>
          You will draw the circles using your mouse. Click at the point you
          want to start drawing and trace the shape around the points you want
          to cluster. Release your mouse after you are done to complete the rest
          of the shape. You can drag beyond the rectangle where the points are
          displayed, so do not feel constrained by the borders.
        </p>
        <p>Click next to continue to practice trials.</p>
        <div>
          <button className="btn btn-primary" onClick={onNext}>
            Begin
          </button>
        </div>
      </div>
    </ScreenCentered>
  );
}

function Uploading() {
  return (
    <ScreenCentered>
      <p>Uploading your data...</p>
    </ScreenCentered>
  );
}

function RealExperimentNotice({ onNext, onStartOver }) {
  return (
    <ScreenCentered>
      <p>If you need more practice or didn't understand the task, please click the button below to start over.</p>
      <p>
        <button
          className="btn btn-secondary"
          onClick={() => {
            onStartOver();
          }}
        >
          Start over
        </button>
      </p>
      <p>If you are ready to begin the experiment, please click the button below to start.</p>
      <p>
        <button
          className="btn btn-primary"
          onClick={() => {
            onNext();
          }}
        >
          Start
        </button>
      </p>
    </ScreenCentered>
  );
}

Instructions.propTypes = {
  onNext: PropTypes.func
};

function Break({ onNext, blockNumber }) {
  const [password, setPassword] = useState("");

  if (blockNumber === 2) {
    return (
      <ScreenCentered>
        <p>You have reached a checkpoint, please contact the experimentor.</p>
        <input
          className="form-control"
          type="password"
          onChange={e => {
            if (e.target.value === "sashank") {
              onNext();
            } else {
              setPassword(e.target.value);
            }
          }}
          value={password}
        />
      </ScreenCentered>
    );
  }

  return (
    <ScreenCentered>
      <p>Please take a break.</p>
      <div>
        <button className="btn btn-primary" onClick={onNext}>
          Next
        </button>
      </div>
    </ScreenCentered>
  );
}

Break.propTypes = {
  onNext: PropTypes.func,
  blockNumber: PropTypes.number
};

function End() {
  return (
    <ScreenCentered>
      <p>Thank you for participating.</p>
    </ScreenCentered>
  );
}

function ExperimentInfo({ onDone }) {
  const [participantId, setParticipantId] = useState("");
  // Due to COVID-19, I had to move this study online. We only want participants to do set 2. 
  const SET_NUMBER = 2;
  // 2020-03-17 const [setNumber, setSetNumber] = useState(1);

  return (
    <ScreenCentered>
      <div>
        <label>Participant ID</label>
        <input
          type="text"
          className="form-control"
          value={participantId}
          onChange={e => setParticipantId(e.target.value)}
        />
      </div>
      <div className="mt-2">
        <button
          className="btn btn-primary"
          onClick={() => {
            if (participantId.length != 0) {
              onDone({
                participantId: participantId,
                setNumber: SET_NUMBER,
                startDateTime: Date.now()
              });
            }
          }}
        >
          Submit
        </button>
      </div>
    </ScreenCentered>
  );
}

ExperimentInfo.propTypes = {
  onDone: PropTypes.func
};

function App() {
  const [stage, setStage] = useState("experimentInfo");
  const experimentInfoRef = useRef({ setNumber: 1 });
  const blockNumberRef = useRef();
  const experimentDataRef = useRef({});

  async function onEnd() {
    experimentInfoRef.current.endDateTime = Date.now();
    const payload = {
      experimentData: experimentDataRef.current,
      experimentInfo: experimentInfoRef.current
    };

    console.log("Payload");
    console.log(payload);

    const closeListener = function(e) {
      var confirmationMessage = "Data is uploading, please wait...";

      (e || window.event).returnValue = confirmationMessage; //Gecko + IE
      return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
    };

    window.addEventListener("beforeunload", closeListener);
    setStage("uploading");

    try {
      await fetch("/api/study-complete", {
        method: "POST",
        body: JSON.stringify(payload),
        headers: {
          "content-type": "application/json"
        }
      });
      window.removeEventListener("beforeunload", closeListener);
      setStage("end");
    } catch (err) {
      console.error(err);
      setStage("error");
    }
  }

  if (stage === "experiment") {
    return (
      <Block
        blockNumber={blockNumberRef.current}
        setNumber={experimentInfoRef.current.setNumber}
        onDone={data => {
          experimentDataRef.current[blockNumberRef.current] = data;
          console.log(`Block ${blockNumberRef.current} data received`, data)
          if (blockNumberRef.current === 4) {
            onEnd();
          } else {
            setStage("break");
          }
        }}
      />
    );
  }

  if (stage === "practice") {
    return (
      <Block
        practice={true}
        onDone={data => {
          experimentDataRef.current["practice"] = data
          setStage("realExperimentNotice");
        }}
      />
    );
  }

  if (stage === "realExperimentNotice") {
    return (
      <RealExperimentNotice
        onNext={() => {
          setStage("experiment");
        }}
        onStartOver={() => {
          setStage("instructions");
        }}
      />
    );
  }

  switch (stage) {
    case "experimentInfo":
      return (
        <ExperimentInfo
          onDone={userExperimentInfo => {
            experimentInfoRef.current = userExperimentInfo;
            setStage("instructions");
          }}
        />
      );
    case "instructions":
      return (
        <Instructions
          onNext={() => {
            blockNumberRef.current = 1;
            setStage("practice");
          }}
        />
      );
    case "break":
      return (
        <Break
          blockNumber={blockNumberRef.current}
          onNext={() => {
            blockNumberRef.current += 1;
            setStage("experiment");
          }}
        />
      );
    case "uploading":
      return <Uploading />;
    case "end":
      return <End />;
    case "error":
      return (
        <ScreenCentered>
          There was an error uploading the data, please call the experimenter.
        </ScreenCentered>
      );
    default:
      return <Instructions />;
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
