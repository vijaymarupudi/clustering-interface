#!/usr/bin/python3

import subprocess
from functools import partial
import sys
from shlex import quote

run = partial(subprocess.run, shell=True, check=True)

try:
    stimuli_directory = sys.argv[1]
except IndexError:
    sys.exit("Please provide directory to stimuli")

run("./scripts/build.sh")

run("rm build/api/stimuli -r")
run(f"cp {quote(stimuli_directory)} build/api/stimuli -r")

# o flag to hide modifying directory times (for /api/data)
run("""
    rsync build/ vijay@clustering.vijaymarupudi.com:/websites/clustering.vijaymarupudi.com/ -rP --filter 'exclude build/api/data' -o
    ssh vijay@clustering.vijaymarupudi.com 'sudo chown clustering /websites/clustering.vijaymarupudi.com/api/data -R && sudo systemctl restart clusteringvijaymarupudicom.service'
""")

