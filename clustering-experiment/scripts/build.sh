#!/usr/bin/env bash

cd client
npx webpack -p
cp -r public/* dist/
cd ..

# removing existing build
rm build -rf
mkdir build
cp client/dist/* build -r
mkdir build/api -p
cp server/* build/api -r
