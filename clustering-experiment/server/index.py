from sanic import Sanic
from sanic.response import file, json as sanic_json
import json
import random
import os
import datetime

app = Sanic()


@app.route("/api/set/<set_>/<block>")
def set_block_info(request, set_, block):
    with open("stimuli/set_block_unique_uuids.json") as f:
        set_block_unique_uuids = json.load(f)

    # shuffle and send
    list_of_unique_uuids = set_block_unique_uuids[set_][block]
    random.shuffle(list_of_unique_uuids)
    return sanic_json(list_of_unique_uuids)

@app.route("/api/practice-set")
def practice_set(request):
    with open("stimuli/practice_uuids.json") as f:
        uuid_list = json.load(f)
    return sanic_json(uuid_list)


@app.route("/api/study-complete", methods=["POST"])
def post_study_data(request):
    # print(request.json)
    os.makedirs("data", exist_ok=True)
    with open(
        os.path.join(
            "data",
            datetime.datetime.now().isoformat()
            + "-"
            + request.json["experimentInfo"]["participantId"]
            + ".json",
        ),
        "w",
    ) as f:
        json.dump(request.json, f)
    return sanic_json({"status": "success"})


@app.route("/api/stimuli/<unique_uuid>")
def specific_stimuli(request, unique_uuid):
    return file("stimuli/stimuli_json/" + unique_uuid + ".json")

if __name__ == "__main__":
    app.run(debug=True)
