library(tidyverse)
library(lme4)
library(car)

raw_data = read_csv("factorial_data.csv") %>% 
  mutate(group = if_else(order == 'None', "Control", "Experimental")) %>% 
  mutate(log_estimation = log(estimation), log_correct_answer = log(correct_answer))

# Reaction times density by number and arithmetic
raw_data %>% filter(group == 'Experimental') %>% 
  ggplot(aes(response_time)) + 
  geom_density() + 
  facet_grid(rows = vars(arithmetic), cols = vars(number))

# Reaction times by number and arithmetic
raw_data %>% filter(group == "Experimental") %>% 
  ggplot(aes(factor(number), response_time)) +
  geom_boxplot() +
  facet_wrap(~ arithmetic)

# Response times by arithmetic
raw_data %>%
  group_by(arithmetic) %>%
  summarize(mean_rt = mean(response_time))

# Response times model
# model = lmer(
#   response_time ~ arithmetic * number * order + (1 + number * arithmetic * order |
#                                            participant_number),
#   raw_data,
#   control = lmerControl(optimizer = "bobyqa", optCtrl = list(maxfun = 10000000))
# )
# model %>% summary()
# Anova(model, type = 3)

# Multiplication: correct answer vs estimation
raw_data %>% filter(group == "Experimental", arithmetic == 'x') %>% 
  ggplot(aes(log_correct_answer, log_estimation, color = factor(participant_number))) +
  geom_point() +
  geom_line(aes(group = participant_number)) +
  facet_grid(~ order)

# Addition: correct answer vs estimation
raw_data %>% filter(group == "Experimental", arithmetic == '+') %>% 
  ggplot(aes(correct_answer, estimation, color = factor(participant_number))) +
  geom_point() +
  geom_line(aes(group = participant_number)) +
  facet_grid(~ order)

log_custom = function(col) {
  if_else(col >= 0, log(col), -log(-1 * col))
}

# Multiplication estimation difference
raw_data %>%
  filter(group == "Experimental") %>%
  select(participant_number, number, estimation, order, arithmetic) %>%
  pivot_wider(names_from = order, values_from = estimation) %>%
  mutate(estimation_difference = desc - asc) %>%
  filter(arithmetic == 'x') %>%
  ggplot(aes(
    number,
    log_custom(estimation_difference),
    group = participant_number,
    color = factor(participant_number)
  )) +
  geom_line() +
  geom_point()

# Addition: Estimation difference
raw_data %>%
  filter(group == "Experimental") %>%
  select(participant_number, number, estimation, order, arithmetic) %>%
  pivot_wider(names_from = order, values_from = estimation) %>%
  mutate(estimation_difference = desc - asc) %>%
  filter(arithmetic == '+') %>%
  ggplot(aes(
    number,
    estimation_difference,
    group = participant_number,
    color = factor(participant_number)
  )) +
  geom_line() +
  geom_point()

model = lmer(log_estimation ~ log_correct_answer + (1 + log_correct_answer|participant_number), raw_data)
model %>% summary()
Anova(model, type = 3)
