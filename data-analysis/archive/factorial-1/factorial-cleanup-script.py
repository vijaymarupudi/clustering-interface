import csv
import math
from pprint import pp
import sys


def cleanup_estimation(raw_estimation_data):
    indexes_of_numbers = []
    index_of_last_number = None
    for idx, item in enumerate(raw_estimation_data):
        if item == "backspace":
            indexes_of_numbers.pop()
        elif item == "return":
            final_string = ""
            for index in indexes_of_numbers:
                final_string += raw_estimation_data[index]
            return int(final_string)
        else:
            try:
                int(item)  # make sure this is an integer
                indexes_of_numbers.append(idx)
                index_of_last_number = idx
            except ValueError:  # not an integer
                print(f"{item} key was ignored.", file=sys.stderr)

def rename_column(dict_, old_name, new_name):
    dict_[new_name] = dict_.pop(old_name)

def correct_answer(arithmetic, order, number):

    number = int(number)

    if order != "None":
        n = number
        final = 1 if arithmetic == 'x' else 0
        while n > 0:
            if arithmetic == "+":
                final += n
            else:
                final *= n
            n -= 1
        return final
    else:
        final = 1 if arithmetic == 'x' else 0
        for _ in range((number * 2) - 1):
            if arithmetic == "+":
                final += number
            else:
                final *= number
        return final


def main():
    # This script will print out the keys it does not deal with to stderr. Make
    # sure that these keys are not keys that have semantic meaning. I took care
    # of enter and backspace, but there might be others. This is just something
    # to be aware of. I can take a look once we have more participants

    # Ensure filename is provided.
    try:
        sys.argv[1]
    except IndexError:
        sys.exit("Provide filename argument")

    print(
        "MAKE SURE THESE KEYS WOULD HAVE BEEN TYPED BY ACCIDENT AND NOT TO EDIT THE NUMBER.",
        file=sys.stderr,
    )


    # This part opens the raw data and cleans it up.
    with open(sys.argv[1]) as file:
        reader = csv.DictReader(file)
        items = list(reader)
        for item in items:
            clean_estimation = cleanup_estimation(
                eval(item["estimation"])
            )
            item["response_time"] = eval(item["response_time"])[-1]
            item["estimation"] = clean_estimation

            # Changes the name of empty column name
            rename_column(item, "", "row")
            rename_column(item, "participant", "participant_number")

            arithmetic = item["arithmetic"]
            number = item["number"]
            order = item["order"]

            answer = correct_answer(arithmetic, order, number)
            item["correct_answer"] = answer

    # Outputting the final file to output.csv. That should be clean data.
    writer = csv.DictWriter(sys.stdout, items[0].keys())
    writer.writeheader()
    writer.writerows(items)

if __name__ == "__main__":
    main()
