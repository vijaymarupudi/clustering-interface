library(tidyverse)
library(broom)

pairs = read_csv("pairs.csv") %>% filter(participant_id != 1)

pairs %>% ggplot(aes(fowlkes_mallows_index)) + geom_histogram()

pairs

mod = lm(fowlkes_mallows_index ~ flipped * group, data = pairs)
glance(mod)
d = aov(fowlkes_mallows_index ~ flipped * group + Error(participant_id), data = pairs)
d %>% summary()


mod = lm(fowlkes_mallows_index ~ number_of_points, data = pairs)
glance(mod)

mod = lm(fowlkes_mallows_index ~ factor(number_of_points), data = pairs)
glance(mod)
TukeyHSD(aov(mod))
