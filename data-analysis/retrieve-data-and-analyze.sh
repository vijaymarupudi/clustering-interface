#!/bin/bash

outliers=('2019-12-06T19:51:17.721782-1.json' '2020-03-04T22:28:44.711288-21.json')

rsync server:/websites/clustering.vijaymarupudi.com/api/data/ data -aP

cd data
rm "${outliers[@]}"
cd ..

python clustering-analysis.py --stimuli-folder ../../clustering-stimuli/stimuli-initial-clustering-experiment/ data/* --save-to-png
