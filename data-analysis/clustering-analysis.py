from dataclasses import dataclass, field
import math
import datetime
import json
from operator import attrgetter
from typing import List, Optional, Dict, Tuple, Set
import argparse
from pathlib import Path
from io import StringIO
import sys
from textwrap import dedent
import uuid
import csv
import cairo

CANVAS_WIDTH = 800
CANVAS_HEIGHT = 500

POINT_ID_DICTIONARY: Dict[Tuple[int, int], str] = {}
STIMULI_POINTS: Dict[str, List["Point"]] = {}

l = list


@dataclass
class Point:
    """
    Represents a point. Has an id unique to the x and y coordinates. id will be
    reused if coordinates match.
    """

    # pylint: disable=invalid-name
    x: int
    y: int
    id_: str = field(init=False)

    def __post_init__(self):
        key = (self.x, self.y)
        preexisting_id = POINT_ID_DICTIONARY.get(key)
        if preexisting_id:
            self.id_ = preexisting_id
        else:
            id_ = str(uuid.uuid4())
            POINT_ID_DICTIONARY[key] = id_
            self.id_ = id_

    def __eq__(self, other: 'Point'):
        return self.id_ == other.id_

    def __hash__(self):
        return hash(self.id_)

    def _flip(self):
        return type(self)(CANVAS_WIDTH - self.x, CANVAS_HEIGHT - self.y)

    def canonical_point(self, is_flipped: bool):
        """
        Returns a new flipped point if flipped
        """
        if is_flipped:
            return self._flip()
        return self

    @classmethod
    def from_dict(cls, dict_):
        """
        Creates a point from a dictionary
        """
        return cls(dict_["x"], dict_["y"])


def canonical_cluster_sets(clusters: List["Cluster"], flipped: bool):
    """
    Canonicalizes points in a cluster and returns a set of points for each
    cluster.
    """
    cluster_sets = []
    for cluster in clusters:
        cluster_points = (p.canonical_point(flipped) for p in cluster.member_points)
        cluster_sets.append(set(cluster_points))
    return cluster_sets


@dataclass
class ClusterEdgePoint(Point):
    """
    Represents a cluster edge point from the dataset
    """
    timestamp: int

    @classmethod
    def from_dict(cls, cluster_point_dict: dict):
        """
        Create from dictionary.
        """
        return cls(
            cluster_point_dict["x"],
            cluster_point_dict["y"],
            cluster_point_dict["timestamp"],
        )


@dataclass
class Cluster:
    """
    Represents the data of one cluster from the dataset.
    """
    cluster_number: int
    edge_points: List[ClusterEdgePoint]
    member_points: Set[Point]

    @classmethod
    def from_dict(
        cls,
        cluster_number: int,
        cluster_dict: dict,
        remaining_stimuli_points: Set[Point],
    ):
        """
        Create from dictionary
        """
        edge_points = [
            ClusterEdgePoint.from_dict(d) for d in cluster_dict["edgePoints"]
        ]

        point_membership_list = cluster_dict["pointMembership"]

        # Of all the points on the stimulus
        member_points_pre = set(
            Point.from_dict(d["point"]) for d in point_membership_list if d["member"]
        )
        # The points of the ones that remain.
        member_points = remaining_stimuli_points.intersection(member_points_pre)
        # Remove member points from the remaining points.
        remaining_stimuli_points.difference_update(member_points)
        # print(point_members_list)
        return cls(cluster_number, edge_points, member_points)


@dataclass
class StimulusData:
    """
    Represents the data of one stimulus item from the dataset
    """
    unique_uuid: str
    base_uuid: str
    flipped: bool
    start_timestamp: int
    end_timestamp: int
    start_date_time: datetime.datetime
    end_date_time: datetime.datetime
    duration: int
    stimulus: dict
    clusters: List[Cluster]
    trial_number: Optional[int]

    def save_to_png(self, path):
        with cairo.ImageSurface(
            cairo.FORMAT_ARGB32, CANVAS_WIDTH, CANVAS_HEIGHT
        ) as surface:
            ctx = cairo.Context(surface)
            ctx.set_source_rgb(1, 1, 1)
            ctx.rectangle(0, 0, 800, 500)
            ctx.fill()
            ctx.set_source_rgb(0, 0, 0)

            for point in self.stimulus["points"]:
                ctx.arc(point["x"], point["y"], 5, 0, math.pi * 2)
                ctx.fill()

            for cluster in self.clusters:
                ctx.new_path()
                sorted_edge_points = sorted(cluster.edge_points, key=attrgetter('timestamp'))
                for edge_point in sorted_edge_points:
                    ctx.line_to(edge_point.x, edge_point.y)
                ctx.close_path()
                ctx.stroke()

            surface.write_to_png(path)

    @classmethod
    def from_dict(cls, unique_uuid: str, stimulus_data_dict: dict):
        # pylint: disable=too-many-locals
        """Create from dictionary"""
        stimulus = stimulus_data_dict["stimulus"]
        base_uuid = stimulus["base_uuid"]
        flipped = stimulus["flipped"]
        start_date_time_v = stimulus_data_dict["startDateTime"]
        start_date_time = datetime.datetime.fromtimestamp(start_date_time_v / 1000)
        start_timestamp = stimulus_data_dict["startTimestamp"]
        end_timestamp = stimulus_data_dict["endTimestamp"]
        duration = end_timestamp - start_timestamp
        end_date_time = start_date_time + datetime.timedelta(milliseconds=duration)

        remaining_stimuli_points = set(STIMULI_POINTS[stimulus["unique_uuid"]])
        clusters = []
        for idx, cluster_dict in enumerate(stimulus_data_dict["clusters"]):
            clusters.append(
                Cluster.from_dict(idx + 1, cluster_dict, remaining_stimuli_points)
            )
        assert not remaining_stimuli_points, "Unclustered points in stimuli?"

        # Removing clusters with no member points
        clusters = [c for c in clusters if c.member_points]

        return cls(
            unique_uuid,
            base_uuid,
            flipped,
            start_timestamp,
            end_timestamp,
            start_date_time,
            end_date_time,
            duration,
            stimulus,
            clusters,
            None
        )


@dataclass
class Block:
    """
    Represents the data for a block
    """
    block_id: str
    stimuli_uuids: List[str]
    stimuli_data: List[StimulusData]

    @classmethod
    def from_dict(cls, block_id: str, block_dict: dict):
        """
        Create from dictionary
        """
        stimuli_uuids = block_dict["blockStimuliList"]
        block_cluster_data_base = block_dict["blockData"]
        stimuli_data = []
        for unique_uuid, stimulus_data in block_cluster_data_base.items():
            stimuli_data.append(StimulusData.from_dict(unique_uuid, stimulus_data))
        return cls(block_id, stimuli_uuids, stimuli_data)


@dataclass
class Experiment:
    """
    Represents the data from the experiment.
    """

    start_date_time: Optional[datetime.datetime]
    end_date_time: datetime.datetime
    participant_id: str
    set_: int
    blocks: List[Block]

    @classmethod
    def from_dict(cls, dict_: dict):
        """
        Create from dictionary
        """

        # The .get and or is due to an old version of the data.
        end_date_time_v = (
            dict_["experimentInfo"].get("endDateTime") or dict_["finishTimestamp"]
        )
        # One version of the data scheme did not have a start date time.
        start_date_time_v = dict_["experimentInfo"].get("startDateTime")

        participant_id = dict_["experimentInfo"]["participantId"]
        set_ = dict_["experimentInfo"]["setNumber"]
        start_date_time = (
            datetime.datetime.fromtimestamp(start_date_time_v / 1000)
            if start_date_time_v
            else None
        )
        end_date_time = datetime.datetime.fromtimestamp(end_date_time_v / 1000)

        blocks = []
        for block_id, block_data in dict_["experimentData"].items():
            blocks.append(Block.from_dict(block_id, block_data))

        return cls(start_date_time, end_date_time, participant_id, set_, blocks)


def calc_clustering_indices(clusters_1: List[Set[Point]], clusters_2: List[Set[Point]]):
    """
    Calculate the Rand and Fowlkes-Mallows indices for the clusters.
    """

    # pylint: disable=invalid-name

    # The meaning of the word cluster changes here. Instead of an instance of
    # the class Cluster, it becomes a set of points. This is to calculate the
    # Rand Index.
    all_points = set(point for cluster in clusters_1 for point in cluster)
    # Using frozenset for hashability and order independence
    pairs_of_points = set(
        frozenset((point1, point2))
        for point1 in all_points
        for point2 in all_points
        if point1 != point2
    )

    # for rand index
    a = 0
    b = 0

    # for fowlkes-mallows
    tp = 0
    fp = 0
    fn = 0

    for pair in pairs_of_points:

        same1 = False
        same2 = False

        for cluster in clusters_1:
            if pair.issubset(cluster):
                same1 = True
                break

        for cluster in clusters_2:
            if pair.issubset(cluster):
                same2 = True
                break

        if same1 and same2:
            a += 1
            tp += 1
        if same1 and not same2:
            fp += 1
        if not same1 and same2:
            fn += 1
        if not same1 and not same2:
            b += 1

    # Perfect rand score
    # return len(pairs_of_points) / (len(all_points) * (len(all_points) - 1) / 2)

    rand = (a + b) / (len(all_points) * (len(all_points) - 1) / 2)
    fowlkes_mallows = ((tp / (tp + fp)) * (tp / (tp + fn))) ** 0.5
    return rand, fowlkes_mallows


def compare_stimuli_data_pair(stim_1: StimulusData, stim_2: StimulusData):
    """
    For a pair of stimuli data, returns data about them.
    """
    cluster_sets_1, cluster_sets_2 = (
        canonical_cluster_sets(stim_1.clusters, stim_1.flipped),
        canonical_cluster_sets(stim_2.clusters, stim_2.flipped),
    )

    number_of_points = stim_1.stimulus["number_of_points"]
    flipped = "flipped" if stim_1.flipped or stim_2.flipped else "same"
    group = stim_1.stimulus["group"]
    rand, fowlkes_mallows = calc_clustering_indices(cluster_sets_1, cluster_sets_2)

    return {
        "base_uuid": stim_1.base_uuid,
        "stim_1": stim_1.unique_uuid,
        "stim_2": stim_2.unique_uuid,
        "stim_1_trial_number": stim_1.trial_number,
        "stim_2_trial_number": stim_2.trial_number,
        "number_of_points": number_of_points,
        "flipped": flipped,
        "group": group,
        "duration_stim_1": stim_1.duration,
        "duration_stim_2": stim_2.duration,
        "number_of_clusters_stim_1": len(stim_1.clusters),
        "number_of_clusters_stim_2": len(stim_2.clusters),
        "rand_index": rand,
        "fowlkes_mallows_index": fowlkes_mallows,
    }


def parse_args():
    """
    Parses command line args.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--stimuli-folder", required=True, help="The folder where the stimuli reside."
    )
    parser.add_argument(
        "data",
        nargs='+',
        help="Path to the json file with the data for a participant.",
    )
    parser.add_argument("--save-to-png", action='store_true', help="Generate PNG files displaying the clusters for each stimulus.")
    return parser.parse_args()

def experiment_process_pairs(experiment):

    final_csv_dicts = []

    stimuli_data_to_compare: Dict[str, List[StimulusData]] = {}

    for block in experiment.blocks:
        for stimulus_data in block.stimuli_data:
            stimuli_data_to_compare.setdefault(stimulus_data.base_uuid, []).append(
                stimulus_data
            )

    for stimulus_list in stimuli_data_to_compare.values():
        if len(stimulus_list) == 1:
            stim_1, = stimulus_list
            assert stim_1.stimulus["practice_stimulus"] # make sure this is a practice stimulus
        elif len(stimulus_list) == 2:
            stim_1, stim_2 = stimulus_list
            csv_dict = compare_stimuli_data_pair(stim_1, stim_2)
            csv_dict["participant_id"] = experiment.participant_id
            final_csv_dicts.append(csv_dict)
        else:
            raise Exception("Three of the same base?")

    return final_csv_dicts

def make_csv(list_, filename):
    with open(filename, 'w', newline='') as f:
        writer = csv.DictWriter(f, list_[0].keys())
        writer.writeheader()
        for item in list_:
            writer.writerow(item)

def main():
    """
    Main coroutine for the program.
    """

    args = parse_args()

    for stimulus_path in (Path(args.stimuli_folder) / "stimuli_json").iterdir():
        stimulus_dict = json.loads(stimulus_path.read_bytes())
        for point in stimulus_dict["points"]:
            STIMULI_POINTS.setdefault(stimulus_dict["unique_uuid"], set()).add(
                Point.from_dict(point)
            )

    data_l = []
    for raw_path in args.data:
        raw_data = json.loads(Path(raw_path).read_bytes())
        data_l.append(raw_data)

    experiments = [Experiment.from_dict(d) for d in data_l]

    # Adding trial numbers
    for experiment in experiments:
        experimental_stimuli_data = []
        for block in experiment.blocks:

            # IMPORTANT: Do not number practice trials
            if block.block_id != "practice":
                experimental_stimuli_data.extend(block.stimuli_data)

        # Sort by start timestamp to get the right order
        experimental_stimuli_data.sort(key=lambda s: s.start_timestamp)

        counter = 0
        for stimulus_data in experimental_stimuli_data:
            stimulus_data.trial_number = counter
            counter += 1

    pairs_csv_dicts = []
    for experiment in experiments:
        pairs_csv_dicts.extend(experiment_process_pairs(experiment))

    clusters_output_dir = Path("participant-clusters")
    clusters_output_dir.mkdir(exist_ok=True)
    single_csv_dicts = []
    clusters_csv_dicts = []
    for experiment in experiments:
        for block in experiment.blocks:
            for stimulus_data in block.stimuli_data:
                image_file_name = f"{experiment.participant_id}_{stimulus_data.unique_uuid}.png"
                if args.save_to_png:
                    stimulus_data.save_to_png(str(clusters_output_dir / image_file_name))
                single_csv_dicts.append({
                    "participant_id": experiment.participant_id,
                    "stimulus": stimulus_data.unique_uuid,
                    "base_uuid": stimulus_data.base_uuid,
                    "trial_number": stimulus_data.trial_number,
                    "group": stimulus_data.stimulus["group"],
                    "flipped": stimulus_data.flipped,
                    "number_of_points": stimulus_data.stimulus["number_of_points"],
                    "number_of_clusters": len(stimulus_data.clusters),
                    "duration": stimulus_data.duration
                })

                for cluster in stimulus_data.clusters:

                    clusters_csv_dicts.append({
                        "participant_id": experiment.participant_id,
                        "stimulus": stimulus_data.unique_uuid,
                        "base_uuid": stimulus_data.base_uuid,
                        "trial_number": stimulus_data.trial_number,
                        "group": stimulus_data.stimulus["group"],
                        "flipped": stimulus_data.flipped,
                        "number_of_points": stimulus_data.stimulus["number_of_points"],
                        "cluster_number": cluster.cluster_number,
                        "number_of_points_in_clusters": len(cluster.member_points)
                    })


    make_csv(pairs_csv_dicts, "pairs.csv")
    make_csv(single_csv_dicts, "singles.csv")
    make_csv(clusters_csv_dicts, "clusters.csv")



if __name__ == "__main__":
    main()
