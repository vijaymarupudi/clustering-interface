import sys

import json
import uuid
import math
from typing import List, Dict, Optional
from itertools import chain
import datetime
from copy import deepcopy
import os
from os import path
import subprocess
import random

import cairo
sys.path.append("build")
import py_random_points

CANVAS_HEIGHT = 500
CANVAS_WIDTH = 800
OUTPUT_DIR = "build/stimuli"

# number_of_points: mean, sd
# simulation details: stimuli_maker.py commit: e61a2c633ed8c1476ec02df29b74c014980ea3da
# 100000 stims per number of points
# These are the means and sds when grouped by number of points.
STD_VZ_TABLE = {
    10: (1.26, 1.03),
    15: (1.04, 1.00),
    20: (0.928, 0.991),
    25: (0.877, 0.979),
    30: (0.864, 0.971),
    35: (0.864, 0.961),
    40: (0.884, 0.956),
    45: (0.911, 0.948),
    50: (0.943, 0.937),
}


class Stimulus:
    def __init__(self, number_of_points, output):
        self.base_uuid: str = output["base_uuid"]
        self.unique_uuid: str = str(uuid.uuid4())
        self.number_of_points: int = number_of_points
        self.z_score: float = output["z_score"]
        self.vacuumed_z_score: float = output["vacuumed_z_score"]
        self.std_vaccumed_z_score: float = std_vz_score(
            number_of_points, self.vacuumed_z_score
        )
        self.points: List[Dict[str, int]] = output["points"]
        self.flipped: bool = False
        self.group: Optional[str] = None
        self.practice_stimulus = False

    def to_dict(self):
        return {
            "base_uuid": self.base_uuid,
            "unique_uuid": self.unique_uuid,
            "number_of_points": self.number_of_points,
            "z_score": self.z_score,
            "vacuumed_z_score": self.vacuumed_z_score,
            "std_vaccumed_z_score": self.std_vaccumed_z_score,
            "flipped": self.flipped,
            "group": self.group,
            "points": self.points,
            "practice_stimulus": self.practice_stimulus
        }

    def __repr__(self):
        return f"<Stimulus n={self.number_of_points}>"

    def flip(self):
        new_stimulus = deepcopy(self)
        new_stimulus.unique_uuid = str(uuid.uuid4())
        new_stimulus.flipped = True
        for point in new_stimulus.points:
            point["x"] = CANVAS_WIDTH - point["x"]
            point["y"] = CANVAS_HEIGHT - point["y"]
        return new_stimulus

    def save_to_png(self, path):
        with cairo.ImageSurface(
            cairo.FORMAT_ARGB32, CANVAS_WIDTH, CANVAS_HEIGHT
        ) as surface:
            ctx = cairo.Context(surface)
            ctx.set_source_rgb(1, 1, 1)
            ctx.rectangle(0, 0, 800, 500)
            ctx.fill()
            ctx.set_source_rgb(0, 0, 0)
            for point in self.points:
                ctx.arc(point["x"], point["y"], 5, 0, math.pi * 2)
                ctx.fill()
            surface.write_to_png(path)

    @classmethod
    def generate(cls, number_of_points):
        return cls(number_of_points, py_random_points.make_points(number_of_points))


def std_vz_score(number_of_points, vzscore):
    mean, sd = STD_VZ_TABLE[number_of_points]
    return (vzscore - mean) / sd


def random_half_split(input_list):
    length = len(input_list)
    assert length % 2 == 0, "List not even"
    list_ = deepcopy(input_list)
    random.shuffle(list_)
    return (list_[0 : length // 2], list_[length // 2 :])


NUM_PER_GROUP = 4


def main():
    # unique_uuid: Stimulus
    stimuli: Dict[str, Stimulus] = {}
    # unique_uuid: info_dict
    stimuli_info: dict = {}
    # unique_uuid: unique_uuid
    flipped_index = {}
    # lists of unique_uuid
    set_block_unique_uuids = {
        1: {1: [], 2: [], 3: [], 4: []},
        2: {1: [], 2: [], 3: [], 4: []},
    }

    non_flipped_list = []
    flipped_list = []

    def flipped_ones(list_of_uuids):
        out = []
        for unique_uuid in list_of_uuids:
            flipped_uuid = flipped_index[unique_uuid]
            out.append(stimuli[flipped_uuid].unique_uuid)
        return out

    for number_of_points in (10, 15, 20, 25, 30):

        clustered: List[Stimulus] = []
        disperse: List[Stimulus] = []

        while len(clustered) < NUM_PER_GROUP:
            stimulus = Stimulus.generate(number_of_points)
            if -2.05 < stimulus.std_vaccumed_z_score < -1.95:
                stimulus.group = "clustered"
                clustered.append(stimulus)

        while len(disperse) < NUM_PER_GROUP:
            stimulus = Stimulus.generate(number_of_points)
            if 0.95 < stimulus.std_vaccumed_z_score < 1.05:
                stimulus.group = "disperse"
                disperse.append(stimulus)

        for stimulus in chain(clustered, disperse):
            non_flipped_list.append(stimulus)
            flipped = stimulus.flip()
            flipped_list.append(flipped)
            flipped_index[stimulus.unique_uuid] = flipped.unique_uuid

        for stimulus in chain(non_flipped_list, flipped_list):
            stimuli[stimulus.unique_uuid] = stimulus
            stimulus_info = deepcopy(stimulus.to_dict())
            del stimulus_info["points"]
            stimuli_info[stimulus.unique_uuid] = stimulus_info

    one_half_uuids, another_half_uuids = random_half_split(non_flipped_list)
    one_half_uuids = [s.unique_uuid for s in one_half_uuids]
    another_half_uuids = [s.unique_uuid for s in another_half_uuids]

    set_1_second_half = deepcopy(one_half_uuids)
    set_1_second_half.extend(flipped_ones(another_half_uuids))
    random.shuffle(set_1_second_half)
    set_2_second_half = deepcopy(another_half_uuids)
    set_2_second_half.extend(flipped_ones(one_half_uuids))
    random.shuffle(set_2_second_half)

    # First half of blocks
    for block in (1, 2):
        same_items = [
            s.unique_uuid
            for s in non_flipped_list[
                (block - 1)
                * len(non_flipped_list)
                // 2 : block
                * len(non_flipped_list)
                // 2
            ]
        ]

        set_block_unique_uuids[1][block] = same_items
        set_block_unique_uuids[2][block] = same_items

    # Second half of blocks stimuli
    for block in (3, 4):
        offset = block - 2
        set_block_unique_uuids[1][block] = set_1_second_half[
            (offset - 1)
            * len(set_1_second_half)
            // 2 : offset
            * len(set_1_second_half)
            // 2
        ]
        set_block_unique_uuids[2][block] = set_2_second_half[
            (offset - 1)
            * len(set_2_second_half)
            // 2 : offset
            * len(set_2_second_half)
            // 2
        ]

    # Verification
    total_unique_items = set()
    non_flipped_uuids = set(s.unique_uuid for s in non_flipped_list)
    for set_ in (1, 2):
        for block in (1, 2, 3, 4):
            items = set_block_unique_uuids[set_][block]
            print(f"Set {set_}, Block {block}, Length {len(items)}")
            total_unique_items.update(items)
            flipped_report = ",".join(
                ["NF" if item in non_flipped_uuids else "F" for item in items]
            )
            print(flipped_report)
    print(f"Total unique experimental items: {len(total_unique_items)}")

    # Practice stimuli

    practice_stimuli = [Stimulus.generate(n) for n in (10, 15, 25)]
    for stim in practice_stimuli:
        stim.practice_stimulus = True
        stimulus_info = deepcopy(stim.to_dict())
        stimuli[stim.unique_uuid] = stim
        del stimulus_info["points"]
        stimuli_info[stim.unique_uuid] = stimulus_info

    # Outputting the files
    os.mkdir(OUTPUT_DIR)
    with open(
        path.join(OUTPUT_DIR, "stimuli_info.json"), "w"
    ) as f:  # pylint: disable=invalid-name
        json.dump(stimuli_info, f)

    with open(
        path.join(OUTPUT_DIR, "set_block_unique_uuids.json"), "w"
    ) as f:  # pylint: disable=invalid-name
        json.dump(set_block_unique_uuids, f)

    with open(
        path.join(OUTPUT_DIR, "practice_uuids.json"), "w"
    ) as f:
        json.dump([s.unique_uuid for s in practice_stimuli], f)

    with open(
        path.join(OUTPUT_DIR, "generation_info.json"), "w"
    ) as f:  # pylint: disable=invalid-name
        json.dump(
            {
                "generation_time": datetime.datetime.now().isoformat(),
                "generation_code_commit_hash": subprocess.check_output(
                    ["git", "rev-parse", "HEAD"]
                )
                .decode()
                .strip(),
            },
            f,
        )

    stimuli_json_dir = path.join(OUTPUT_DIR, "stimuli_json")
    os.mkdir(stimuli_json_dir)
    for unique_uuid, stimulus in stimuli.items():
        with open(
            path.join(stimuli_json_dir, unique_uuid + ".json"), "w"
        ) as f:  # pylint: disable=invalid-name
            json.dump(stimulus.to_dict(), f)

    stimuli_images_dir = path.join(OUTPUT_DIR, "stimuli_images")
    os.mkdir(stimuli_images_dir)
    for unique_uuid, stimulus in stimuli.items():
        filepath = path.join(stimuli_images_dir, unique_uuid + ".png")
        stimulus.save_to_png(filepath)


if __name__ == "__main__":
    main()
